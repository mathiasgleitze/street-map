# street-map

Software to process and display cartographic data in .OSM format, written in Java. 


## Features

* Display a map the size of Denmark.
* Zooming and panning.
* Calculate and display the shortest route between to addresses for pedestrians, bicycles, and cars.
* Show closest streetname to cursor.
* Autocomplete address names.
* Loading maps in .OSM format.
* Saving loaded maps to binary format, for subsequently faster loading.
* Adding points of interest.

![UI](https://gitlab.com/mathiasghoffmann/street-map/-/raw/main/img/ui-example.png)
