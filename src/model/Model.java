package model;

import helpers.*;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
import osm.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipInputStream;
import static java.lang.Integer.parseInt;

public class Model extends Observable implements Serializable {
    private EnumMap<OSMWayType, List<Shape>> shapes = initializeEnumMap();
    private EnumMap<OSMWayType, KDTree> trees = new EnumMap<OSMWayType, KDTree>(OSMWayType.class);
    private ArrayList<OSMRoad> roads = new ArrayList<>();
    private WeightedGraph graph;
    private AddressMap addresses = new AddressMap();
    private double minLat, minLon, maxLat, maxLon;
    private double lonFactor;
    private ArrayList<PointOfInterest> pointsOfInterest = new ArrayList<>();
    public Model() {
    }

    public Model(String filename) {
        load(filename);
    }

    private EnumMap<OSMWayType, List<Shape>> initializeEnumMap() {
        EnumMap<OSMWayType, List<Shape>> map = new EnumMap<OSMWayType, List<Shape>>(OSMWayType.class);
        for (OSMWayType type: OSMWayType.values()) {
            map.put(type, new ArrayList<>());
        }
        return map;
    }

    public void add(OSMWayType type, Shape shape) {
        shapes.get(type).add(shape);
        dirty();
    }

    public void addPointOfInterest(Point2D p, String s) {
        pointsOfInterest.add(new PointOfInterest(p, s));
    }

    public void deletePointOfInterest(int index) {
        pointsOfInterest.remove(index);
    }

    public void changeText(int index, String text) {
        pointsOfInterest.get(index).setText(text);
    }

    public PointOfInterest getPointOfInterest(int index) {
        return pointsOfInterest.get(index);
    }

    public ArrayList<PointOfInterest> getPointsOfInterest() {
        return pointsOfInterest;
    }

    public void dirty() {
        setChanged();
        notifyObservers();
    }

    public void readFromOSM(InputSource filename) {
        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            xmlReader.setContentHandler(new OSMHandler());
            xmlReader.parse(filename);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("All model-data is parsed and created!");
    }

    /**
     * Saves the data serialized needed to create the map.
     * Called by the file chooser.
     *
     * @param filename The name of the file
     */
    public void save(String filename) {
        try {
            int bufferSize = 5 * 1024;
            ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filename), bufferSize));
            os.writeObject(trees);
            System.out.println("Trees saved!");
            os.writeObject(graph);
            System.out.println("Graph saved!");
            os.writeObject(addresses);
            System.out.println("Addresses saved!");
            os.writeObject(pointsOfInterest);
            os.writeObject(minLon);
            os.writeObject(minLat);
            os.writeObject(maxLon);
            os.writeObject(maxLat);
            os.writeObject(lonFactor);
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        JOptionPane.showMessageDialog(null, "Fully saved");
    }

    /**
     * Loads the file with the given name (+ path).
     * Can load from ".osm", ".zip" and ".bin".
     *
     * @param filename Name/path of the file that needs to open
     */
    public void load(String filename) {
        if (filename.endsWith(".osm")) {
            readFromOSM(new InputSource(filename));
        } else if (filename.endsWith(".zip")) {
            try {
                ZipInputStream zis = new ZipInputStream(new FileInputStream(filename));
                zis.getNextEntry();
                readFromOSM(new InputSource(zis));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (filename.endsWith(".bin")) {
            int bufferSize = 5 * 1024;
            ObjectInputStream is = null;
            try {
                is = new ObjectInputStream(new BufferedInputStream(new FileInputStream(filename), bufferSize));
            } catch (IOException e) {
                e.printStackTrace();
            }
            loadObjects(is);
        }
        dirty();
    }

    /**
     * Loads a given file from the resources.
     * Called when opening the default map from jar.
     *
     * @param filename Name/path of the file that needs to open
     */
    public void jarLoad(String filename) {
        ObjectInputStream is = null;
        int bufferSize = 5 * 1024;
        try {
            is = new ObjectInputStream(new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream(filename), bufferSize));
        } catch (IOException e) {
            e.printStackTrace();
        }
        loadObjects(is);
        dirty();
    }

    private void loadObjects(ObjectInputStream is) {
        try {
            trees = (EnumMap<OSMWayType, KDTree>) is.readObject();
            System.out.println("Trees loaded!");
            graph = (WeightedGraph) is.readObject();
            System.out.println("graph loaded!");
            addresses = (AddressMap) is.readObject();
            System.out.println("addresses loaded!");
            pointsOfInterest = (ArrayList<PointOfInterest>) is.readObject();
            minLon = (double) is.readObject();
            minLat = (double) is.readObject();
            maxLon = (double) is.readObject();
            maxLat = (double) is.readObject();
            lonFactor = (double) is.readObject();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        dirty();
    }

    public Iterable<Shape> get(OSMWayType type, Rectangle2D viewRect) {
        return trees.get(type).visibleShapes(viewRect);
    }

    public ArrayList<PointOfInterest> getPointsOfInterrest() {
        return pointsOfInterest;
    }

    public double getMinLat() {
        return minLat;
    }

    public double getMinLon() {
        return minLon;
    }

    public double getMaxLat() {
        return maxLat;
    }

    public double getMaxLon() {
        return maxLon;
    }

    public double getLonFactor() {
        return lonFactor;
    }

    public WeightedGraph getGraph() {
        return graph;
    }

    public AddressMap getAddresses() {
        return addresses;
    }

    private class OSMHandler extends DefaultHandler {
        LongToOSMNodeMap idToNode = new LongToOSMNodeMap(25);
        Map<Long, OSMWay> idToWay = new HashMap<>();
        HashMap<OSMNode, OSMWay> coastlines = new HashMap<>();
        OSMWay way;
        ArrayList<Long> wayids;
        private StringBuilder address = new StringBuilder();
        private DistanceCalculator distanceCalculator = new DistanceCalculator();
        private OSMWayType type;
        private OSMRelation relation;
        private String placename;
        private boolean isOneWay;
        private boolean isRoad;
        private boolean isDrivingAllowed;
        private boolean isCyclingAllowed;
        private boolean isWalkingAllowed;
        private double maxSpeed;
        private boolean traffic_signals;
        private long endNodeId;
        private boolean addCity;
        private boolean addAdress;
        private boolean addIsland;
        private boolean addIslet;
        private boolean isTunnel;
        private long nodeId;
        double lon;
        double lat;

        String addrCity;
        String addrStreet;
        String addrPostCode;
        String addrHouseNumber;

        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            switch (qName) {
                case "bounds":
                    minLat = Double.parseDouble(attributes.getValue("minlat"));
                    minLon = Double.parseDouble(attributes.getValue("minlon"));
                    maxLat = Double.parseDouble(attributes.getValue("maxlat"));
                    maxLon = Double.parseDouble(attributes.getValue("maxlon"));
                    double avgLat = minLat + (maxLat - minLat) / 2;
                    lonFactor = Math.cos(avgLat / 180 * Math.PI);
                    minLon *= lonFactor;
                    maxLon *= lonFactor;
                    maxLat = -maxLat;
                    minLat = -minLat;
                    break;
                case "node":
                    lon = Double.parseDouble(attributes.getValue("lon"));
                    lat = Double.parseDouble(attributes.getValue("lat"));
                    nodeId = Long.parseLong(attributes.getValue("id"));
                    idToNode.put(nodeId, lonFactor * lon, -lat);
                    break;
                case "way":
                    way = new OSMWay();
                    wayids = new ArrayList<>();
                    type = OSMWayType.UNKNOWN;
                    idToWay.put(Long.parseLong(attributes.getValue("id")), way);
                    break;
                case "relation":
                    relation = new OSMRelation();
                    type = OSMWayType.UNKNOWN;
                    break;
                case "member":
                    OSMWay w = idToWay.get(Long.parseLong(attributes.getValue("ref")));
                    if (w != null) {
                        relation.add(w);
                    }
                    break;
                case "tag":
                    switch (attributes.getValue("k")) {
                        case "addr:city":
                            addrCity = attributes.getValue("v");
                            addAdress = true;
                            break;
                        case "addr:housenumber":
                            addrHouseNumber = attributes.getValue("v");
                            break;
                        case "addr:postcode":
                            addrPostCode = attributes.getValue("v");
                            break;
                        case "addr:street":
                            addrStreet = attributes.getValue("v");
                            break;
                        case "name":
                            this.placename = attributes.getValue("v");
                            break;
                        case "oneway":
                            if (attributes.getValue("v").equals("yes"))
                                isOneWay = true;
                            break;
                        case "maxspeed":
                            matcher = pattern.matcher(attributes.getValue("v"));
                            if(matcher.matches()) {
                                maxSpeed = Double.parseDouble(attributes.getValue("v"));
                            }
                            break;
                        case "place":
                            addCity = true;
                            switch (attributes.getValue("v")) {
                                case "city":
                                    type = OSMWayType.CITY;
                                    break;
                                case "suburb":
                                    type = OSMWayType.SUBURB;
                                    break;
                                case "island":
                                    addIsland = true;
                                    addCity = false;
                                    break;
                                case "islet":
                                    addIslet = true;
                                    addCity = false;
                                    break;
                                case "town":
                                    type = OSMWayType.TOWN;
                                    break;
                                case "village":
                                    type = OSMWayType.VILLAGE;
                                    break;
                                default:
                                    addIslet = false;
                                    addCity = false;
                                    break;
                            }
                            break;
                        case "highway":
                            type = OSMWayType.ROAD;
                            isRoad = true;
                            switch (attributes.getValue("v")) {
                                case "motorway":
                                    type = OSMWayType.MOTORWAY;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = false;
                                    isWalkingAllowed = false;
                                    break;
                                case "motorway_link":
                                    type = OSMWayType.MOTORWAY_LINK;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = false;
                                    isWalkingAllowed = false;
                                    break;
                                case "trunk":
                                    type = OSMWayType.TRUNK;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = false;
                                    isWalkingAllowed = false;
                                    break;
                                case "trunk_link":
                                    type = OSMWayType.TRUNK_LINK;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = false;
                                    isWalkingAllowed = false;
                                    break;
                                case "primary":
                                    type = OSMWayType.PRIMARY;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = false;
                                    break;
                                case "primary_link":
                                    type = OSMWayType.PRIMARY_LINK;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = false;
                                    break;
                                case "secondary":
                                    type = OSMWayType.SECONDARY;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "secondary_link":
                                    type = OSMWayType.SECONDARY_LINK;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "tertiary":
                                    type = OSMWayType.TERTIARY;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "tertiary_link":
                                    type = OSMWayType.TERTIARY_LINK;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "unclassified":
                                    type = OSMWayType.UNCLASSIFIED;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "residential":
                                    type = OSMWayType.RESIDENTIAL;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "living_street":
                                    type = OSMWayType.LIVING_STREET;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "service":
                                    type = OSMWayType.SERVICE;
                                    isDrivingAllowed = true;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "pedestrian":
                                    type = OSMWayType.PEDESTRIAN;
                                    isDrivingAllowed = false;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "footway":
                                    type = OSMWayType.FOOTWAY;
                                    isDrivingAllowed = false;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "bridleway":
                                    type = OSMWayType.BRIDLEWAY;
                                    isDrivingAllowed = false;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "path":
                                    type = OSMWayType.FOOTWAY;
                                    isDrivingAllowed = false;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "steps":
                                    type = OSMWayType.FOOTWAY;
                                    isDrivingAllowed = false;
                                    isCyclingAllowed = false;
                                    isWalkingAllowed = true;
                                    break;
                                case "cycleway":
                                    type = OSMWayType.CYCLEWAY;
                                    isDrivingAllowed = false;
                                    isCyclingAllowed = true;
                                    isWalkingAllowed = true;
                                    break;
                                case "traffic_signals":
                                    isRoad = false;
                                    traffic_signals = true;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "waterway":
                            switch (attributes.getValue("v")) {
                                case "river":
                                    type = OSMWayType.RIVER;
                                    break;
                                case "canal":
                                    type = OSMWayType.RIVER;
                                    break;
                                case "stream":
                                    type = OSMWayType.STREAM;
                                    break;
                                case "drain":
                                    type = OSMWayType.STREAM;
                                    break;
                                case "ditch":
                                    type = OSMWayType.STREAM;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "aeroway":
                            switch (attributes.getValue("v")) {
                                case "runway":
                                    type = OSMWayType.RUNWAY;
                                    break;
                                case "taxiway":
                                    type = OSMWayType.TAXIWAY;
                                    break;
                                case "taxilane":
                                    type = OSMWayType.TAXILANE;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "natural":
                            switch (attributes.getValue("v")) {
                                case "water":
                                    type = OSMWayType.WATER;
                                    break;
                                case "coastline":
                                    type = OSMWayType.COASTLINE;
                                    break;
                                case "grassland":
                                    type = OSMWayType.GRASS;
                                    break;
                                case "wood":
                                    type = OSMWayType.FOREST;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "leisure":
                            switch (attributes.getValue("v")) {
                                case "park":
                                    type = OSMWayType.GRASS;
                                    break;
                                case "garden":
                                    type = OSMWayType.GRASS;
                                    break;
                                case "playground":
                                    type = OSMWayType.GRASS;
                                    break;
                                case "pitch":
                                    type = OSMWayType.GRASS;
                                    break;
                                case "golf_course":
                                    type = OSMWayType.GRASS;
                                    break;
                                case "miniature_golf":
                                    type = OSMWayType.GRASS;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "landuse":
                            switch (attributes.getValue("v")) {
                                case "grass":
                                    type = OSMWayType.GRASS;
                                    break;
                                case "residential":
                                    type = OSMWayType.RESIDENTIAL_AREA;
                                    break;
                                case "forest":
                                    type = OSMWayType.FOREST;
                                    break;
                                case "farmland":
                                    type = OSMWayType.FARMLAND;
                                    break;
                                case "commercial":
                                    type = OSMWayType.COMMERCIAL;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case "building":
                            type = OSMWayType.BUILDING;
                            break;
                        case "man_made":
                            if (attributes.getValue("v").equals("bridge")) {
                                type = OSMWayType.BRIDGE;
                            }
                            break;
                        case "railway":
                            type = OSMWayType.RAILWAY;
                            break;
                        case "route":
                            if(attributes.getValue("v").equals("ferry")) {
                                type = OSMWayType.FERRY;
                            }
                        case "tunnel":
                            if (attributes.getValue("v").equals("yes")) {
                                isTunnel = true;
                            }
                        default:
                            break;
                    }
                    break;
                case "nd":
                    endNodeId = Long.parseLong(attributes.getValue("ref"));
                    way.add(idToNode.get(endNodeId));
                    wayids.add(endNodeId);
                    break;
                default:
                    break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            Path2D path = new Path2D.Double();
            OSMNode node;
            switch (qName) {
                case "way":
                    if(addIslet) {
                        Point2D p = new Point2D.Double(way.from().getLon(), way.from().getLat());
                        OSMPlaceName islet = new OSMPlaceName(p, placename);
                        add(OSMWayType.ISLET, islet);
                        addIslet = false;
                    }
                    if(addIsland) {
                        Point2D p = new Point2D.Double(way.from().getLon(), way.from().getLat());
                        OSMPlaceName island = new OSMPlaceName(p, placename);
                        add(OSMWayType.ISLAND, island);
                        addIsland = false;
                    }
                    if (isTunnel) {
                        if (type == OSMWayType.RAILWAY) {type = OSMWayType.RAILWAYTUNNEL;}
                        if (type == OSMWayType.MOTORWAY) {type = OSMWayType.MOTORWAYTUNNEL;}
                    }
                    if (type == OSMWayType.COASTLINE) {
                        OSMWay before = coastlines.remove(way.from());
                        OSMWay after = coastlines.remove(way.to());
                        OSMWay merged = new OSMWay();

                        // Add these three paths together
                        if (before != null){
                            merged.addAll(before.subList(0, before.size() -1));
                        }
                        merged.addAll(way);
                        if (after != null && after != before) {
                            merged.addAll(after.subList(1, after.size()));
                        }
                        coastlines.put(merged.to(), merged);
                        coastlines.put(merged.from(), merged);
                    } else {
                        if (isRoad) {
                            if (placename.isEmpty()) {
                                placename = type.toString();
                                placename = placename.charAt(0) + placename.substring(1).toLowerCase();
                            }
                            for(int i = 1; i < way.size(); i++){
                                OSMRoad road = new OSMRoad(type, placename, isOneWay);
                                Point2D from = new Point2D.Double(way.get(i - 1).getLon(), way.get(i - 1).getLat());
                                Point2D to   = new Point2D.Double(way.get(i).getLon(), way.get(i).getLat());
                                if(maxSpeed == 0) {
                                    switch (road.type) {
                                        case MOTORWAY:
                                            maxSpeed = 130.0;
                                            break;
                                        case MOTORWAYTUNNEL:
                                            maxSpeed = 130.0;
                                            break;
                                        case MOTORWAY_LINK:
                                            maxSpeed = 50.0;
                                            break;
                                        case TRUNK:
                                            maxSpeed = 90.0;
                                            break;
                                        case TRUNK_LINK:
                                            maxSpeed = 50.0;
                                            break;
                                        case PRIMARY:
                                            maxSpeed = 80.0;
                                            break;
                                        case PRIMARY_LINK:
                                            maxSpeed = 50.0;
                                            break;
                                        case SECONDARY:
                                            maxSpeed = 60.0;
                                            break;
                                        case SECONDARY_LINK:
                                            maxSpeed = 50.0;
                                            break;
                                        case TERTIARY:
                                            maxSpeed = 50.0;
                                            break;
                                        case TERTIARY_LINK:
                                            maxSpeed = 50.0;
                                            break;
                                        case SERVICE:
                                            maxSpeed = 50.0;
                                            break;
                                        case UNCLASSIFIED:
                                            maxSpeed = 50.0;
                                            break;
                                        case RESIDENTIAL:
                                            maxSpeed = 50.0;
                                            break;
                                        case LIVING_STREET:
                                            maxSpeed = 30;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                road.maxSpeed = maxSpeed;
                                road.isDrivingAllowed = isDrivingAllowed;
                                road.isCyclingAllowed = isCyclingAllowed;
                                road.isWalkingAllowed = isWalkingAllowed;
                                road.from    = from;
                                road.to      = to;
                                road.weight  = distanceCalculator.distance(from, to);
                                road.startNodeId = wayids.get(i - 1);
                                road.endNodeId = wayids.get(i);
                                road.startNodeTrafficSignal = way.get(i - 1).traffic_signals;
                                road.endNodeTrafficSignal = way.get(i).traffic_signals;
                                roads.add(road);
                            }
                            isRoad = false;
                            isDrivingAllowed = false;
                            isCyclingAllowed = false;
                            isWalkingAllowed = false;
                            isTunnel = false;
                            maxSpeed = 0.0;
                        }
                        node = way.get(0);
                        path.moveTo(node.getLon(), node.getLat());
                        for (int i = 1; i < way.size(); i++) {
                            node = way.get(i);
                            path.lineTo(node.getLon(), node.getLat());
                        }
                        isOneWay = false;
                        placename = "";
                        add(type, path);
                    }
                    break;
                case "relation":
                    for (OSMWay way: relation) {
                        node = way.get(0);
                        path.moveTo(node.getLon(), node.getLat());
                        for (int i = 1; i < way.size(); i++) {
                            node = way.get(i);
                            path.lineTo(node.getLon(), node.getLat());
                        }
                    }
                    if(addIsland) {
                        Point2D p = new Point2D.Double(path.getBounds2D().getCenterX(),path.getBounds2D().getCenterY());
                        OSMPlaceName island = new OSMPlaceName(p, placename);
                        Rectangle2D.Double rectangle = (Rectangle2D.Double) path.getBounds2D();
                        double rectangleSize = ((rectangle.height * 100) + (rectangle.width * 100));
                        if(rectangleSize > 20) {
                            add(OSMWayType.BIGISLAND, island);
                        }
                        else if(rectangleSize > 2) {
                            add(OSMWayType.ISLAND, island);
                        } else {
                            add(OSMWayType.ISLET, island);

                        }
                        addIsland = false;
                    }
                    add(type, path);
                    placename = "";
                    break;

                case "node":
                    if(addCity) {
                        Point2D p = new Point2D.Double(lonFactor * lon, -lat);
                        OSMPlaceName city = new OSMPlaceName(p, placename);
                        add(type, city);
                        addCity = false;
                    }
                    if(addAdress) {
                        address.append(addrStreet);
                        address.append(" ");
                        address.append(addrHouseNumber);
                        address.append(", ");
                        address.append(addrPostCode);
                        address.append(" ");
                        address.append(addrCity);
                        OSMAddress a = new OSMAddress(address.toString(), idToNode.get(nodeId));
                        addresses.put(a);
                        address = new StringBuilder();
                        addAdress = false;
                    }
                    if(traffic_signals) {
                        node = idToNode.get(nodeId);
                        node.traffic_signals = true;
                    }
                    traffic_signals = false;
                    type = null;
                    placename = "";
                    break;

                case "osm":
                    for (Map.Entry<OSMNode, OSMWay> coastline: coastlines.entrySet()) {
                        OSMWay way = coastline.getValue();
                        if (coastline.getKey() == way.from()) {
                            path = new Path2D.Double();
                            path.setWindingRule(Path2D.WIND_EVEN_ODD);
                            node = way.get(0);
                            path.moveTo(node.getLon(), node.getLat());
                            for (int i = 1; i < way.size(); i++) {
                                node = way.get(i);
                                path.lineTo(node.getLon(), node.getLat());
                            }
                            add(OSMWayType.COASTLINE, path);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        @Override
        public void endDocument() {
            System.out.println("Everything is parsed!");
            double startTime = System.currentTimeMillis();
            for (EnumMap.Entry<OSMWayType, List<Shape>> entry : shapes.entrySet()) {
                trees.put(entry.getKey(), new KDTree(entry.getValue(), 30));
            }
            double endTime   = System.currentTimeMillis();
            double totalTime = (endTime - startTime) / 1000;
            System.out.println("All trees are created! With time: " + totalTime + " seconds");
            shapes = null;
            System.gc();
            System.out.println("Now creating the graph, with " + roads.size() + " edges!");
            startTime = System.currentTimeMillis();
            graph = new WeightedGraph(roads);
            endTime   = System.currentTimeMillis();
            totalTime = (endTime - startTime) / 1000;
            System.out.println("The graph is created! With time: " + totalTime + " seconds");
            roads = null;
        }
    }
}
