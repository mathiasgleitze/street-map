package model;

import helpers.DistanceCalculator;

import javax.swing.*;
import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * Computes the shortest path between two vertices.
 */
public class ShortestPath {
    private PriorityQueue<WeightedGraph.Vertex> priorityQueue;
    private ArrayList<WeightedGraph.Vertex> vertices;

    public ShortestPath(Model m) {
        this.vertices = m.getGraph().getVertices();
    }

    /**
     * Finds the shortest path between two vertices depending on the vehicle and whether the user wants the fastest or shortest route.
     * @param source the source vertex
     * @param destination the destination vertex
     * @param buttonText the text from one of the vehicle buttons (car/bike/walk) on the user interface
     * @param routeText the text from one of the route buttons (fastest/shortest)
     * @return the list of edges between the source and destination on the route
     */
    public ArrayList<WeightedGraph.Edge> findPath(WeightedGraph.Vertex source, WeightedGraph.Vertex destination, String buttonText,
                                      String routeText) {
        // If fastest route is chosen the factor used to divide the estimationToDestination is set to 130
        double factor;
        if(buttonText.equals("Car") && routeText.equals("Fastest route")) {
            factor = 130;
        }
        else {
            factor = 1;
        }
        DistanceCalculator distanceCalculator = new DistanceCalculator();

        /*
        The loop below sets the value of distTo to POSITIVE_INFINITY and the value of edgeTo to null for all vertices
        as well as the estimation to the destination from the vertex to the distance from the vertex to the destination
        in a straight line. The estimation to the destination depends on whether the user chooses fastest/shortest route.
        */
        for (WeightedGraph.Vertex v : vertices) {
            v.distTo = Double.POSITIVE_INFINITY;
            v.edgeTo = null;
            v.estimationToDestination = distanceCalculator.distance(v.coordinate, destination.coordinate) / factor;
            v.visited = false;
        }
        source.distTo = 0.0;

        // The vertices in the priority queue are sorted according to the sum of their distTo and estimationToDestination
        priorityQueue = new PriorityQueue<>(vertices.size());
        priorityQueue.add(source);
        while(!priorityQueue.isEmpty()) {
            WeightedGraph.Vertex v = priorityQueue.poll();
            if(v.visited) {
                continue;
            }
            v.visited = true;
            // The loop is stopped, when we reach the destination, as we will have found the shortest path between source and destination
            if (v != destination) {
                // The outgoing edges for the vertex will be relaxed, if and only the specific vehicle is allowed on the edge/road
                for (WeightedGraph.Edge e : v) {
                    if(e.isDrivingAllowed && buttonText.equals("Car")) {
                        if(e.isOneWay) {
                           if(v.equals(e.startVertex)) {
                               relax(e, v, factor);
                           }
                        }
                        else {
                            relax(e, v, factor);
                        }
                    }
                    else if(e.isCyclingAllowed && buttonText.equals("Bike")) {
                        relax(e, v, factor);
                    }
                    else if(e.isWalkingAllowed && buttonText.equals("Walk")) {
                        relax(e, v, factor);
                    }
                }
            } else {
                break;
            }
        }

        // Initializing ArrayList to hold all the edges from source to destination
        ArrayList<WeightedGraph.Edge> edgesToDestination = new ArrayList<>();

        // If statement checks, if there is a route to the destination
        if(destination.edgeTo != null) {
            // The shortest path is followed from destination to source by using the edgeTo of every vertex, and
            // every edge on the way is added to the list
            WeightedGraph.Vertex v = destination;
            while(v != source) {
                edgesToDestination.add(v.edgeTo);
                if(v == v.edgeTo.startVertex) {
                    v = v.edgeTo.endVertex;
                }
                else {
                    v = v.edgeTo.startVertex;
                }
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "No route found :-(");
        }

        return edgesToDestination;
    }

    /**
     * Relaxes the edge
     * @param e the edge to be relaxed
     * @param ver the last vertex added to the tree, that has <code>e</code> as an outgoing edge
     * @param factor the factor dependant on whether the route chosen is the fastest or shortest
     */
    private void relax(WeightedGraph.Edge e, WeightedGraph.Vertex ver, double factor) {
        // The edges are not directed, so it is important to know we relax the edge using the correct vertex values
        WeightedGraph.Vertex v = e.startVertex;
        WeightedGraph.Vertex w = e.endVertex;
        if (w == ver){
            w = v;
            v = ver;
        }
        // Shortest path in terms on distance
        if(factor == 1) {
            if(w.distTo > v.distTo + e.weight) {
                w.distTo = v.distTo + e.weight;
                w.edgeTo = e;
                priorityQueue.add(w);
            }
        }
        // Shortest path in terms of time
        else {
            if(w.distTo > v.distTo + e.timeToTraverse) {
                w.distTo = v.distTo + e.timeToTraverse;
                w.edgeTo = e;
                priorityQueue.add(w);
            }
        }
    }
}
