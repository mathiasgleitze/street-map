package model;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a KD-tree.
 * This is used to find elements within a certain area of the map.
 *
 * @author Emil Kastholm Fischer
 */
public class KDTree implements Serializable{
    private Node root;
    private int leafSize;
    private ArrayList<Shape> closestShapes;
    private ArrayList<Shape> visibles;
    private ArrayList<Shape> shapes;

    /**
     * The contructor for the KD-tree.
     * This is called whenever the KD-tree need to initiated.
     * Creates the root and then builds the rest og the tree.
     *
     * @param shapes    The list of shapes of which the tree is build.
     * @param leafSize  The maximum number of shapes that a leaf can represent.
     */
    public KDTree(List<Shape> shapes, int leafSize) {
        this.leafSize = leafSize;
        if (shapes.isEmpty())
            return;

        this.shapes = new ArrayList<>(shapes);

        root = new Node(true);
        buildTree(this.shapes.toArray(new Shape[shapes.size()]), root, 0, shapes.size()-1);
    }

    /**
     * Represents a vertex in the KD-tree.
     * Note that there is two split values - this is to make sure all shapes are found when range searching.
     */
    public class Node implements Serializable{
        double leftSplit;
        double rightSplit;
        boolean isVertical;

        // When the node represents a horizontal line, left is down and right is up
        Node left   = null;
        Node right  = null;

        // Most leafs should represent shapes
        ArrayList<Shape> shapes = null;

        public Node(boolean isVertical) {
            this.isVertical = isVertical;
        }
    }

   /**
    * This method is called whenever one or more elements needs
    * to be found within a certain area of the map
    *
    * @param viewRect  The area containing the needed element(s)
    * @return Returns a list of the shapes within the given rectangle.
    */
    public ArrayList<Shape> visibleShapes(Rectangle2D viewRect) {
        visibles = new ArrayList();
        if (root == null)
            return visibles;
        findVisibleShapes(root, viewRect);
        return visibles;
    }

   /**
    * This method is only called by <code>visibleShapes</code>.
    * The method recursivly range-searches in the KD-tree.
    *
    * @param node       The current node in the search
    * @param viewRect   The area containing the needed element(s)
    */
    private void findVisibleShapes(Node node, Rectangle2D viewRect) {
        if (node.shapes != null) {
            for (Shape shape: node.shapes) {
                if (shape.intersects(viewRect))
                    visibles.add(shape);
            }
            return;
        }

        if (node.isVertical) {
            if (viewRect.getX() < node.leftSplit)       // is the viewRect to the left of the leftSplit?
                findVisibleShapes(node.left, viewRect);
            if (viewRect.getMaxX() > node.rightSplit)   // is the viewRect to the right of the leftSplit?
                findVisibleShapes(node.right, viewRect);
        } else {
            if (viewRect.getY() < node.leftSplit)       // is the viewRect under the leftSplit?
                findVisibleShapes(node.left, viewRect);
            if (viewRect.getMaxY() > node.rightSplit)   // is the viewRect above the rightSplit?
                findVisibleShapes(node.right, viewRect);
        }
    }

    /**
     * Get the shapes closest to the given coordinate.
     *
     * @param x     the x-coordinate.
     * @param y     the y-coordinate.
     * @return      returns a list of the closest shapes to the coordinate.
     */
    public ArrayList<Shape> getClosestShapes(double x, double y) {
        closestShapes = new ArrayList<>();
        if (root == null)
            return closestShapes;
        findClosestShapes(root, x, y);
        return closestShapes;
    }

    /**
     * Only called by <code>getClosestShapes</code>.
     * Recursivly ranges searches in the KD-tree.
     *
     * @param node  The current node in the range search.
     * @param x     The x-coordinate.
     * @param y     The y-coordinate.
     */
    private void findClosestShapes(Node node, double x, double y) {
        if (node.shapes != null) {
            closestShapes.addAll(node.shapes);
            return;
        }

        if (node.isVertical) {
            if ((x) < node.leftSplit)
                findClosestShapes(node.left, x, y);
            if ((x) > node.rightSplit)
                findClosestShapes(node.right, x, y);
        } else {
            if ((y) < node.leftSplit)
                findClosestShapes(node.left, x, y);
            if ((y) > node.rightSplit)
                findClosestShapes(node.right, x, y);
        }
    }

    /**
     * Only called by the constructor.
     * Builds the tree recursivly.
     *
     * @param shapes    The array of shapes that the tree is build over.
     * @param parent    The parent of the current node.
     * @param min       The starting index of the 'sublist' of shapes
     * @param max       the ending index of the 'sublist' of shapes
     */
    private void buildTree(Shape[] shapes, Node parent, int min, int max) {
        int n = max - min;

        // Sets the leafs to represent shapes
        if (n <= leafSize) {
            Shape[] leaf = Arrays.copyOfRange(shapes, min, max);
            parent.shapes = new ArrayList<>(Arrays.asList(leaf));
            return;
        }

        if (parent.isVertical){
            Arrays.sort(shapes, min, max, this::xComparator);
            parent.leftSplit = leftXSplit(shapes, min+n/2);
            parent.rightSplit = rightXSplit(shapes, min+n/2, max);

            parent.left  = new Node(false);
            buildTree(shapes, parent.left, min, n/2+min);

            parent.right = new Node(false);
            buildTree(shapes, parent.right, n/2+min, max);
        }else {
            Arrays.sort(shapes, min, max, this::yComparator);
            parent.leftSplit = leftYSplit(shapes, n/2+min);
            parent.rightSplit = rightYSplit(shapes, n/2+min, max);

            parent.left  = new Node(true);
            buildTree(shapes, parent.left, min, min+n/2);

            parent.right = new Node(true);
            buildTree(shapes, parent.right, n/2+min, max);
        }
    }

    /**
     * Comparators for sorting points according to either x- or y-coordinates.
     *
     * @param s1    used to comparing whether s1 is greater, equal or less than s2.
     * @param s2    used to comparing whether s1 is greater, equal or less than s2.
     * @return      Returns an negative number if s1<s2, zero if s1=s2 and a positiv number if s1>s2.
     */
    private int xComparator(Shape s1, Shape s2) {
        return Double.compare(s1.getBounds2D().getMaxX(), s2.getBounds2D().getMaxX());
    }

    private int yComparator(Shape s1, Shape s2) {
        return Double.compare(s1.getBounds2D().getMaxY(), s2.getBounds2D().getMaxY());
    }

    /**
     * Used to finding the split values for the nodes.
     * Note that the list are already sorted in the left x and y split methods
     *
     * @param shapes    Array of shapes where the split value needs to be found.
     * @param max       The largest index in the (sub-)array.
     * @return          Returns the split values.
     */
    private double leftXSplit(Shape[] shapes, int max) {
        return shapes[max-1].getBounds2D().getMaxX();
    }

    private double rightXSplit(Shape[] shapes, int min, int max) {
        double minimum = shapes[min].getBounds2D().getX();

        for (int i = min; i < max; i++){
            if (shapes[i].getBounds2D().getX() < minimum)
                minimum = shapes[i].getBounds2D().getX();
        }
        return minimum;
    }

    private double leftYSplit(Shape[] shapes, int max) {
        return shapes[max-1].getBounds2D().getMaxY();
    }

    private double rightYSplit(Shape[] shapes, int min, int max) {
        double minimum = shapes[min].getBounds2D().getY();
        for (int i = min; i < max; i++){
            if (shapes[i].getBounds2D().getY() < minimum)
                minimum = shapes[i].getBounds2D().getY();
        }

        return minimum;
    }
}
