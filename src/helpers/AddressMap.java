package helpers;

import osm.OSMAddress;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Map of addresses sorted alphabetically.
 * Meaning if you try to get an address starting with 'A', you will get a list af all addresses starting with 'A'.
 * Danish letters (Æ, Ø, Å) are located at index 26, 27, 28 and non-alphabetical characters are located at index 29.
 * List structure:
 *                  index 0: List of addresses starting with 'A'
 *                  index 1: List of addresses starting with 'B'
 *                  .
 *                  .
 *                  .
 *                  index 29: List of addresses starting with non-alphabetical chars (and non-danish letters)
 *
 * @author Emil Kastholm Fischer
 */
public class AddressMap implements Serializable {
    private ArrayList<ArrayList<OSMAddress>> list;

    public AddressMap() {
        list = new ArrayList<>();
        for (int i = 0; i < 30; i++)
            list.add(new ArrayList<>());
    }

    public ArrayList<OSMAddress> get(char c) {
        int index = indexOfChar(c);
        return list.get(index);
    }

    public ArrayList<OSMAddress> get(String s) {
        int index = indexOfChar(s.charAt(0));
        return list.get(index);
    }

    public void put(OSMAddress address) {
        int index = indexOfChar(address.address.charAt(0));
        list.get(index).add(address);
    }

    private int indexOfChar(char c) {
        char input = Character.toUpperCase(c);
        int index;

        if (input == 'Æ')
            index = 26;
        else if (input == 'Ø')
            index = 27;
        else if (input == 'Å')
            index = 28;
        else
            index = (int) input - 65; // Converts ASCII numbering to alphabetic position (zero based)

        if (index < 0 || index > 29)
            index = 29;

        return index;
    }
}
