package helpers;

import java.awt.geom.Point2D;

/**
 * A simple class with a single method to calculate bearing.
 */
public class BearingCalculator {

    public BearingCalculator() {
    }

    /**
     * Calculates the bearing between two real coordinates, longitude and latitude.
     * @param p1 the first coordinate set.
     * @param p2 the second coordinate set.
     * @return the bearing between the two coordinate sets.
     */
    public double calculateBearing(Point2D p1, Point2D p2) {
        double long1 = p1.getX();
        double long2 = p2.getX();
        double lat1 = Math.toRadians(p1.getY());
        double lat2 = Math.toRadians(p2.getY());
        double longDif = Math.toRadians(long2 - long1);
        double y = Math.sin(longDif) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(longDif);
        double bearing = (Math.toDegrees(Math.atan2(y, x)) + 360)%360;
        return bearing;
    }
}
