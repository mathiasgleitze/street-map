package helpers;

import java.awt.geom.Point2D;
import java.lang.*;
/**
 * A simple class with a single method to calculate distance.
 */
public class DistanceCalculator {

    public DistanceCalculator() {
    }

    /**
     * Calculates the distance between two real coordinates, longitude and latitude.
     * @param p1 the first coordinate set.
     * @param p2 the second coordinate set.
     * @return the distance between the two coordinate sets.
     */
    public double distance(Point2D p1, Point2D p2) {
        double lat1r = Math.toRadians(p1.getY());
        double lat2r = Math.toRadians(p2.getY());
        double l = Math.toRadians((p2.getY() - p1.getY()));
        double m = Math.toRadians((p2.getX() - p1.getX()));
        double a = Math.sin(l/2) * Math.sin(l/2)
                + Math.cos(lat1r) * Math.cos(lat2r)
                * Math.sin(m/2) * Math.sin(m/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = 6371 * c;
        return d;
    }
}