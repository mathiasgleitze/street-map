package view;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Small window with text and a gif, that lets the user know that the map is loading.
 */
public class LoadingPopUp {
    private JFrame frame;

    /**
     * Constructs a new LoadingPopUp.
     */
    public LoadingPopUp() {
        makeFrame();
    }

    private void makeFrame() {
        frame = new JFrame("Førsteårsprojekt");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout(0, 13));
        frame.getContentPane().setBackground(new Color(255,255,255));
        frame.setPreferredSize(new Dimension(350, 200));

        URL url = this.getClass().getClassLoader().getSystemResource("loading.gif");
        ImageIcon imageIcon = new ImageIcon(url);
        JLabel gif = new JLabel(imageIcon);
        gif.setOpaque(true);
        gif.setBackground(new Color(255,255,255));

        JLabel loading = new JLabel("Loading the map, please wait");
        loading.setOpaque(true);
        loading.setBackground(new Color(255,255,255));
        loading.setHorizontalAlignment(JLabel.CENTER);

        JLabel label = new JLabel();
        label.setOpaque(true);
        label.setBackground(new Color(255,255,255));

        frame.add(gif, BorderLayout.CENTER);
        frame.add(loading, BorderLayout.NORTH);
        frame.add(label, BorderLayout.SOUTH);

        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * Returns the frame of this LoadingScreen.
     * @return the frame of this LoadingScreen.
     */
    public JFrame getFrame() {
        return frame;
    }
}
