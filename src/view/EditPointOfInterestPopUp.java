package view;

import model.Model;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 * A window to edit a point of interest.
 */
public class EditPointOfInterestPopUp extends JFrame implements ActionListener{
    Model model;
    CanvasView canvas;
    int pointIndex;
    final int maxLength = 40;

    /**
     * Constructs a EditPointOfInterestPopUp.
     * @param pointIndex is the index in Models ArrayList<pointOfInterest> pointsOfInterest,
     *                   of the point of interest that is to be edited.
     */
    public EditPointOfInterestPopUp(Model m, CanvasView cv, int pointIndex) {
        super("Edit Point Of Interest");
        this.model = m;
        this.canvas = cv;
        this.pointIndex = pointIndex;

        JTextField description = new JTextField(
                model.getPointOfInterest(pointIndex).getText());
        JButton deleteButton = new JButton("Delete point");
        JButton saveButton = new JButton("Save changes");
        description.setSize(200, 40);
        description.setEditable(true);

        this.setLayout(new BorderLayout());
        this.add(description, BorderLayout.NORTH);
        this.add(deleteButton, BorderLayout.CENTER);
        this.add(saveButton, BorderLayout.SOUTH);
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setLocation(MouseInfo.getPointerInfo().getLocation());

        /* A listener that checks if the description / name, is empty or too long, if it is the user
            will get a notification. Otherwise the point of interest will be made.*/
        saveButton.addActionListener(e -> {
            if (!description.getText().equals("")) {
                if (description.getText().length() <= maxLength) {
                    model.changeText(pointIndex, description.getText());
                    canvas.repaint();
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(null,
                            "Your point of interest's name, has to be less than " +  maxLength + " characters.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "The point name can not be empty.");
            }
                });
        deleteButton.addActionListener(e -> {
            model.deletePointOfInterest(pointIndex);
            canvas.repaint();
            this.dispose();
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
