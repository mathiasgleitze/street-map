package view;

import helpers.DistanceCalculator;
import helpers.PointOfInterest;
import helpers.PointOfInterestShape;
import model.Model;
import osm.OSMPlaceName;
import osm.OSMWayType;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.*;

/**
 * A class to render the map.
 */
public class CanvasView extends JComponent implements Observer{
    private final Model model;
    private DistanceCalculator dc;
    private Toolbar tb;
    private AffineTransform transform = new AffineTransform();
    private ArrayList<PointOfInterestShape> drawnPointsOfInterest;
    private Map<String, Color> Colors;
    private boolean useAntiAliasing;
    private boolean drawSearchPoint;
    private boolean drawRoads;
    private boolean drawPointsOfInterest = true;
    boolean maxZoomLVLON;
    private double fps = 0.0;
    private double currentZoomLVL;
    private Path2D route;
    final private double minZoomLVL = 0.1;
    private double zoomBarNumber;
    final static float dash1[] = {0.0002f};
    final static float dash2[] = {0.00001f};
    private Shape searchPoint;
    private Point2D routeEndPoint;

    public CanvasView(Model m, Toolbar tb) {
        this.model = m;
        this.dc = new DistanceCalculator();
        this.tb = tb;
        model.addObserver(this);
        setFocusable(true);
        setFocusable(true);
        setColorsToStandard();
    }

    @Override
    public void paint(Graphics _g) {
        zoomUpdate();
        drawnPointsOfInterest = new ArrayList<>();
        long t1 = System.nanoTime();
        Graphics2D g = (Graphics2D) _g;
        AffineTransform af = g.getTransform();
        g.setStroke(new BasicStroke(Float.MIN_VALUE));
        BasicStroke dashed = new BasicStroke(0.00002f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0);
        BasicStroke dotted = new BasicStroke(0.00001f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash2, 0);
        BasicStroke basicStroke = new BasicStroke(0.00001f);

        Rectangle2D viewRect = new Rectangle2D.Double(0, 0, getWidth(), getHeight());
        g.setPaint(Colors.get("water"));
        g.fill(viewRect);
        g.transform(transform);

        try {
            viewRect = transform.createInverse().createTransformedShape(viewRect).getBounds2D();
        } catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        }

        if (currentZoomLVL <= 3) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
        }

        if (useAntiAliasing) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
        }
        g.setStroke(basicStroke);
        g.setPaint(Colors.get("ground"));
        for (Shape coastline: model.get(OSMWayType.COASTLINE, viewRect)) {
            g.fill(coastline);
        }


        g.setPaint(Colors.get("water"));
        if (currentZoomLVL < 30) {
            for (Shape water: model.get(OSMWayType.WATER, viewRect)) {
                g.fill(water);
            }
        }


        g.setPaint(Colors.get("grass"));
        if (currentZoomLVL < 20) {
            for (Shape grass : model.get(OSMWayType.GRASS, viewRect)) {
                g.fill(grass);
            }
        }

        g.setPaint(Colors.get("forest"));
        if (currentZoomLVL < 20) {
            for (Shape forest : model.get(OSMWayType.FOREST, viewRect)) {
                g.fill(forest);
            }
        }

        g.setPaint(Colors.get("farmland"));
        if (currentZoomLVL < 20) {
            for (Shape farmland : model.get(OSMWayType.FARMLAND, viewRect)) {
                g.fill(farmland);
            }
        }

        g.setPaint(Colors.get("building"));
        if (currentZoomLVL > 35 && currentZoomLVL < 80) {
            for (Shape residential : model.get(OSMWayType.RESIDENTIAL_AREA, viewRect)) {
                g.fill(residential);
            }
        }

        g.setPaint(Colors.get("building"));
        if (currentZoomLVL > 12 && currentZoomLVL < 80) {
            for (Shape commercial : model.get(OSMWayType.COMMERCIAL, viewRect)) {
                g.fill(commercial);
            }
        }
        g.setStroke(new BasicStroke(0.00005f));
        g.setPaint(Colors.get("water"));
        if (currentZoomLVL < 20) {
            for (Shape river : model.get(OSMWayType.RIVER, viewRect)) {
                g.draw(river);
            }
        }
        g.setStroke(new BasicStroke(0.00002f));
        g.setPaint(Colors.get("water"));
        if (currentZoomLVL < 5) {
            for (Shape stream : model.get(OSMWayType.STREAM, viewRect)) {
                g.draw(stream);
            }
        }
        g.setStroke(new BasicStroke(0.00005f));
        g.setPaint(Colors.get("building"));
        if (currentZoomLVL < 12) {
            for (Shape building : model.get(OSMWayType.BUILDING, viewRect)) {
                g.fill(building);
            }
        }
        g.setStroke(new BasicStroke(0.00002f));
        g.setPaint(Colors.get("building"));
        for (Shape bridge : model.get(OSMWayType.BRIDGE, viewRect)) {
            g.fill(bridge);
        }

        g.setPaint(Colors.get("moterwaytunnel"));
        if (currentZoomLVL < 400) {
            g.setStroke(new BasicStroke(0.0001f));
            for (Shape motorwaytunnel : model.get(OSMWayType.MOTORWAYTUNNEL, viewRect)) {
                g.draw(motorwaytunnel);
            }
        }

        g.setPaint(Colors.get("railwaytunnel"));
        if (currentZoomLVL < 30) {
            g.setStroke(new BasicStroke(0.00002f));
            if(currentZoomLVL < 10) {g.setStroke(dashed);}
            for (Shape railwaytunnel : model.get(OSMWayType.RAILWAYTUNNEL, viewRect)) {
                g.draw(railwaytunnel);
            }
        }

        g.setPaint(Colors.get("railway"));
        if (currentZoomLVL < 40) {
            g.setStroke(new BasicStroke(0.00002f));
            if(currentZoomLVL < 10) {g.setStroke(dashed);}
            for (Shape railway : model.get(OSMWayType.RAILWAY, viewRect)) {
                g.draw(railway);
            }
        }

        g.setStroke(new BasicStroke(0.00002f));
        g.setPaint(Color.white);
        if (currentZoomLVL < 5) {
            for (Shape road : model.get(OSMWayType.ROAD, viewRect)) {
                g.draw(road);
            }
            for (Shape unclassified : model.get(OSMWayType.UNCLASSIFIED, viewRect)) {
                g.draw(unclassified);
            }
            for (Shape service : model.get(OSMWayType.SERVICE, viewRect)) {
                g.draw(service);
            }
            for (Shape residential : model.get(OSMWayType.RESIDENTIAL, viewRect)) {
                g.draw(residential);
            }
        }
        g.setPaint(Color.LIGHT_GRAY);
        if (currentZoomLVL < 5) {
            for (Shape living_street : model.get(OSMWayType.LIVING_STREET, viewRect)) {
                g.draw(living_street);
            }
        }
        g.setPaint(Color.GRAY);
        if (currentZoomLVL < 5) {
            for (Shape pedestrian : model.get(OSMWayType.PEDESTRIAN, viewRect)) {
                g.draw(pedestrian);
            }
        }

        g.setStroke(new BasicStroke(0.00007f));
        g.setPaint(Colors.get("tertiary"));
        if (currentZoomLVL < 40) {
            for (Shape tertiary : model.get(OSMWayType.TERTIARY, viewRect)) {
                g.draw(tertiary);
            }
            for (Shape tertiary_link : model.get(OSMWayType.TERTIARY_LINK, viewRect)) {
                g.draw(tertiary_link);
            }
        }
        g.setStroke(new BasicStroke(0.00007f));
        g.setPaint(Colors.get("secondary"));
        if (currentZoomLVL < 100) {
            for (Shape secondary : model.get(OSMWayType.SECONDARY, viewRect)) {
                g.draw(secondary);
            }
            for (Shape secondary_link : model.get(OSMWayType.SECONDARY_LINK, viewRect)) {
                g.draw(secondary_link);
            }
        }
        g.setStroke(new BasicStroke(0.00007f));
        g.setPaint(Colors.get("primary"));
        if (currentZoomLVL < 200) {
            for (Shape primary : model.get(OSMWayType.PRIMARY, viewRect)) {
                g.draw(primary);
            }
            for (Shape primary_link : model.get(OSMWayType.PRIMARY_LINK, viewRect)) {
                g.draw(primary_link);
            }
        }
        g.setStroke(new BasicStroke(0.0001f));
        g.setPaint(Colors.get("trunk"));
        if (currentZoomLVL < 200) {
            for (Shape trunk : model.get(OSMWayType.TRUNK, viewRect)) {
                g.draw(trunk);
            }
            for (Shape trunk_link : model.get(OSMWayType.TRUNK_LINK, viewRect)) {
                g.draw(trunk_link);
            }
        }

        g.setStroke(new BasicStroke(0.0001f));
        g.setPaint(Colors.get("motorway"));
        if (currentZoomLVL < 400) {
            for (Shape motorway : model.get(OSMWayType.MOTORWAY, viewRect)) {
                g.draw(motorway);
            }
            for (Shape motorway_link : model.get(OSMWayType.MOTORWAY_LINK, viewRect)) {
                g.draw(motorway_link);
            }
        }
        g.setStroke(dotted);
        g.setPaint(Colors.get("cycleway"));
        if (currentZoomLVL < 0.9) {
            for (Shape cycleway : model.get(OSMWayType.CYCLEWAY, viewRect)) {
                g.draw(cycleway);
            }
        }
        g.setStroke(dotted);
        g.setPaint(Colors.get("footway"));
        if (currentZoomLVL < 0.5) {
            for (Shape footway : model.get(OSMWayType.FOOTWAY, viewRect)) {
                g.draw(footway);
            }
        }
        g.setPaint(Colors.get("bridleway"));
        if (currentZoomLVL < 0.5) {
            for (Shape bridleway : model.get(OSMWayType.BRIDLEWAY, viewRect)) {
                g.draw(bridleway);
            }
        }
        g.setStroke(new BasicStroke(0.0002f));
        g.setPaint(Colors.get("airport"));
        if (currentZoomLVL < 30) {
            for (Shape taxiway : model.get(OSMWayType.TAXIWAY, viewRect)) {
                g.draw(taxiway);
            }
        }
        g.setStroke(new BasicStroke(0.0001f));
        g.setPaint(Colors.get("airport"));
        if (currentZoomLVL < 30) {
            for (Shape taxiway : model.get(OSMWayType.TAXILANE, viewRect)) {
                g.draw(taxiway);
            }
        }
        g.setStroke(new BasicStroke(0.0005f));
        g.setPaint(Colors.get("airport"));
        if (currentZoomLVL < 30) {
            for (Shape runway : model.get(OSMWayType.RUNWAY, viewRect)) {
                g.draw(runway);
            }
        }

        g.setStroke(new BasicStroke(0.00002f));
        g.setPaint(Colors.get("ferry"));
        if (currentZoomLVL < 40) {
            for (Shape ferry : model.get(OSMWayType.FERRY, viewRect)) {
                g.draw(ferry);
            }
        }

        if (drawSearchPoint) {
            g.setStroke(basicStroke);
            g.setColor(Color.black);
            g.draw(searchPoint);
            g.setColor(Colors.get("green"));
            g.fill(searchPoint);
            drawSearchPoint = false;
        }

        if (drawRoads) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            g.setStroke(new BasicStroke((float) currentZoomLVL / 30000));
            g.setPaint(Colors.get("route"));
            g.draw(route);
            Shape startPointShape = makePointShape(route.getCurrentPoint());
            g.setStroke(basicStroke);
            g.setColor(Color.black);
            g.draw(startPointShape);
            g.setColor(Colors.get("green"));
            g.fill(startPointShape);

            Shape routeEndPointShape = makePointShape(routeEndPoint);
            g.setColor(Color.black);
            g.draw(routeEndPointShape);
            g.setColor(Colors.get("red"));
            g.fill(routeEndPointShape);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_OFF);
        }

        if (drawPointsOfInterest) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            ArrayList<PointOfInterest> pointsofinterest = model.getPointsOfInterest();
            g.setStroke(new BasicStroke(0.0000005f));
            for (int i = 0; i < pointsofinterest.size(); i++) {
                if (viewRect.contains(pointsofinterest.get(i).getPoint())) {
                    Shape s = makePointOfInterestShape(pointsofinterest.get(i).getPoint(), i);
                    g.setPaint(Color.BLACK);
                    g.draw(s);
                    g.setPaint(Colors.get("pointofinterest"));
                    g.fill(s);
                }
            }
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_OFF);
        }

        //Anything drawn with Screen coordinates (not actual coordinates) should be below this point
        g.setTransform(af);

        g.setStroke(new BasicStroke(0.004f));
        g.setPaint(Color.black);
        if (currentZoomLVL > 35 && currentZoomLVL < 400) {
            for (Shape city : model.get(OSMWayType.CITY, viewRect)) {
                drawPlaceName(g, city);
            }
        }

       if (currentZoomLVL > 1 && currentZoomLVL < 35) {
            for (Shape suburb : model.get(OSMWayType.SUBURB, viewRect)) {
                if(!(suburb instanceof Path2D)) {
                    drawPlaceName(g, suburb);
                }
            }
        }

        if (currentZoomLVL > 1 && currentZoomLVL < 100) {
            for (Shape town : model.get(OSMWayType.TOWN, viewRect)) {
                if(!(town instanceof Path2D)) {
                    drawPlaceName(g, town);
                }
            }
        }

        if (currentZoomLVL > 1 && currentZoomLVL < 15) {
            for (Shape village : model.get(OSMWayType.VILLAGE, viewRect)) {
                if(!(village instanceof Path2D)) {
                    drawPlaceName(g, village);
                }
            }
        }

        if (currentZoomLVL > 35 && currentZoomLVL < 150) {
            for (Shape bigisland : model.get(OSMWayType.BIGISLAND, viewRect)) {
                if(!(bigisland instanceof Path2D) ) {
                    drawPlaceName(g, bigisland);
                }
            }
        }

        if (currentZoomLVL > 1 && currentZoomLVL < 35) {
            for (Shape island : model.get(OSMWayType.ISLAND, viewRect)) {
                if(!(island instanceof Path2D)) {
                    drawPlaceName(g, island);
                }
            }
        }

        if (currentZoomLVL > 1 && currentZoomLVL < 5) {
            for (Shape islet : model.get(OSMWayType.ISLET, viewRect)) {
                if(!(islet instanceof Path2D)) {
                    drawPlaceName(g, islet);
                }
            }
        }

        long t2 = System.nanoTime();
        fps = (fps + 1e9/ (t2 - t1)) / 2;

        // Changing the font
        Font defaultFont = g.getFont();
        g.setFont(new Font("Helvetica", Font.PLAIN, 10));

        // Drawing the retangle to hold the fps counter
        g.setColor(Color.WHITE);
        Rectangle2D fpsRectangle = new Rectangle2D.Double(5, 5, 80, 20);
        g.fill(fpsRectangle);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g.setColor(Color.BLACK);
        g.draw(fpsRectangle);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Drawing the fps counter
        g.drawString(String.format("FPS: %.1f", fps), 8, 18);

        // Changing back to the default font
        g.setFont(defaultFont);

        // draws the zoombar
        g.setColor(Color.BLACK);
        g.setStroke(new BasicStroke(1.5f));
        double factor = (currentZoomLVL * 1000) / zoomBarNumber;
        double lineLeftXCoordinate = getWidth() - getWidth() / factor - 5;
        Line2D zoomBar = new Line2D.Double((lineLeftXCoordinate), (getHeight() - 5), (getWidth() - 5), (getHeight() - 5));
        // creates the parts that stick up on the zoombar
        Line2D dotleft = new Line2D.Double((lineLeftXCoordinate), (getHeight() - 5), (lineLeftXCoordinate), (getHeight() - 10));
        Line2D dotright = new Line2D.Double((getWidth() - 5), (getHeight() - 5), (getWidth() - 5), (getHeight() - 10));
        g.draw(zoomBar);
        g.draw(dotleft);
        g.draw(dotright);
    }

    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param o   the observable object.
     * @param arg an argument passed to the <code>notifyObservers</code>
     */
    @Override
    public void update(Observable o, Object arg) {
        repaint();
    }

    public void toggleAntiAliasing() {
        useAntiAliasing = !useAntiAliasing;
        repaint();
    }

    public void togglePointsOfInterest() {
        drawPointsOfInterest = !drawPointsOfInterest;
        repaint();
    }

    public void togglePointsOfInterestOn() {
        drawPointsOfInterest = true;
        repaint();
    }

    /**
     * Draws a route.
     * @param drawRoads A boolean on wether or not to draw the route.
     * @param route A Path2D route, where the last moveTo() is the start point of the route.
     * @param destination The destination model coordinates.
     */
    public void drawRoute(boolean drawRoads, Path2D route, Point2D destination) {
        this.route = route;
        this.drawRoads = drawRoads;
        this.routeEndPoint = destination;
        repaint();
    }

    /**
     * Pans to map to screen coordinates.
     * @param dx Screen x coordinate (Has to be the negative value of the actual coordinate)
     * @param dy Screen y coordinate (Has to be the negative value of the actual coordinate)
     */
    public void pan(double dx, double dy) {
        transform.preConcatenate(AffineTransform.getTranslateInstance(dx, dy));
        repaint();
    }

    public void zoomToCenter(double factor) {
        zoom(factor, -getWidth() /2, -getHeight() / 2);
    }

    public void zoom(double factor, double x, double y) {
        if (zoomAllowed(factor)) {
            pan(x, y);
            transform.preConcatenate(AffineTransform.getScaleInstance(factor, factor));
            pan(-x, -y);
            zoomUpdate();
            repaint();
        }
    }
    // Checks wether or not the user is within bounds, so they dont zoom to far in or out.
    public boolean zoomAllowed(double factor) {
        if (factor > 1 && currentZoomLVL > minZoomLVL) {
            return true;
        } else if (factor < 1 && currentZoomLVL < 600) {
            return true;
        } else if (!maxZoomLVLON) {
            maxZoomLVLON = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Transforms screen coordinates to model coordinates (not to be confused with real coordinates).
     * @param p Screen coordinates.
     * @return Model coordinates.
     */
    public Point2D toModelCoords(Point2D p) {
        try {
            Point2D p1 = transform.inverseTransform(p, null);
            p1 = new Point2D.Double(p1.getX(), p1.getY());
            return p1;
        } catch (NoninvertibleTransformException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Transforms model coordinates to screen coordinates.
     * @param p Model coordinates
     * @return Screen coordinates.
     */
    public Point2D toScreenCoords(Point2D p) {
        return transform.transform(p, null);
    }
    /**
     * Transforms screen coordinates to real coordinates.
     * @param p Screen coordinates.
     * @return Real coordinates.
     */
    public Point2D toRealCoords(Point2D p) {
        Point2D result = toModelCoords(p);
        if (result != null) {
            return new Point2D.Double(result.getX()/model.getLonFactor(), result.getY() * - 1);
        }
        return null;
    }

    /**
     * Updates the current zoom level, and updates the zoombars value.
     */
    public void zoomUpdate() {
        Point2D p1 = new Point2D.Double(1.0, getHeight() * 1.0);
        Point2D p2 = new Point2D.Double(getWidth() * 1.0, getHeight() * 1.0);
        Point2D p1Real = toRealCoords(p1);
        Point2D p2Real = toRealCoords(p2);
        currentZoomLVL = dc.distance(p1Real, p2Real);
        // dividing by 5 for a fifth of the screen and multiply by 1000 to get meters = just multiplying by 200
        zoomBarNumber = currentZoomLVL * 200;
        if (zoomBarNumber < 50000 && zoomBarNumber > 5000) {
            zoomBarNumber = Math.round(zoomBarNumber / 1000.0) * 1000;
        } else if (zoomBarNumber < 5000 && zoomBarNumber > 1000) {
            zoomBarNumber = Math.round(zoomBarNumber / 250.0) * 250;
        } else if (zoomBarNumber < 1000 && zoomBarNumber > 100) {
            zoomBarNumber = Math.round(zoomBarNumber / 50.0) * 50;
        } else if (zoomBarNumber < 100) {
            zoomBarNumber = Math.round(zoomBarNumber / 10.0) * 10;
        } else {
            zoomBarNumber = Math.round(zoomBarNumber / 10000.0) * 10000;
        }
        tb.setZoomNumber(zoomBarNumber);
    }

    /**
     * Method to draw names as strings on the map.
     * @param g The paint methods Graphics2D object.
     * @param place A shape (OSMPlaceName) with the information to draw the string.
     */
    public void drawPlaceName(Graphics2D g, Shape place) {
        OSMPlaceName tmp = (OSMPlaceName) place;
        Point2D p = new Point2D.Double(tmp.x1, tmp.y1);
        p = transform.transform(p, null);
        float px = (float) p.getX();
        float py = (float) p.getY();

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setFont(new Font("Helvetica", Font.PLAIN, 13));
        g.drawString(tmp.getPlaceName(), px, py);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_OFF);
    }

    /**
     * Method to make a pointer shape on the map.
     * @param p1 the place on the map, in model coordinates, to draw the shape.
     * @return the pointer shape
     */
    public Shape makePointShape(Point2D p1) {
        Path2D.Double path = new Path2D.Double();
        p1 = transform.transform(p1, null);
        Point2D p2 = new Point2D.Double(p1.getX() - 10, p1.getY() - 15);
        Point2D p3 = new Point2D.Double(p1.getX(), p1.getY() - 20);
        Point2D p4 = new Point2D.Double(p1.getX() + 10, p1.getY() - 15);
        p1 = toModelCoords(p1);
        p2 = toModelCoords(p2);
        p3 = toModelCoords(p3);
        p4 = toModelCoords(p4);
        path.moveTo(p1.getX(), p1.getY());
        path.lineTo(p2.getX(), p2.getY());
        path.lineTo(p3.getX(), p3.getY());
        path.lineTo(p4.getX(), p4.getY());
        path.lineTo(p1.getX(), p1.getY());
        return path;
    }

    public void drawSearchPoint(Point2D p) {
        if (p == null)
            return;
        this.searchPoint = makePointShape(p);
        drawSearchPoint = true;
        repaint();
    }
    /**
     * Method to make a marker, for a point of interest.
     * @param p1 the points coordinates in model coordinates.
     * @param indexinpointsofinterest the index in the ArrayList pointsOfInterest, of the point of point.
     * @return the shape of the marker.
     */
    public Shape makePointOfInterestShape(Point2D p1, int indexinpointsofinterest) {
        Shape path = makePointShape(p1);
        drawnPointsOfInterest.add(new PointOfInterestShape(path, indexinpointsofinterest));
        return path;
    }

    // Returns a list of the points of interest currently drawn.
    public ArrayList<PointOfInterestShape> getDrawnPointsOfInterest() {
        return drawnPointsOfInterest;
    }

    //New colors should be added here.
    public void setColorsToStandard() {
        Colors = new HashMap<>();
        Color water = new Color(92, 168, 255);
        Color ground = new Color(190,210,145);
        Color grass = new Color(159, 255, 128);
        Color forest = new Color(119, 201, 42);
        Color farmland = new Color(255,235, 153);
        Color building = new Color( 172,  169, 151);
        Color ferry = new Color(60, 100, 170);
        Color route = new Color(238, 78, 190);
        Color alternativeroute = new Color(223,153, 0);
        Color footway = new Color(223, 153, 0);
        Color cycleway = new Color(35, 113, 223);
        Color motorway = new Color(240, 0, 0);
        Color trunk = new Color(255, 165, 0);
        Color primary = new Color(240, 230, 140);
        Color secondary = new Color(240, 230, 140);
        Color tertiary = new Color(255, 255, 255);
        Color railwaytunnel = new Color(0, 0, 139);
        Color railway = new Color(30, 30, 30);
        Color pointofinterest = new Color(44,	156,	145);
        Color bridleway = new Color(163, 223, 94);
        Color airport = new Color(130, 130, 130);
        Color moterwaytunnel = new Color(180, 20, 20);
        Color green = new Color(0, 128, 0);
        Colors.put("water", water);
        Colors.put("ground", ground);
        Colors.put("grass", grass);
        Colors.put("forest", forest);
        Colors.put("farmland", farmland);
        Colors.put("building", building);
        Colors.put("ferry", ferry);
        Colors.put("route", route);
        Colors.put("alternativeroute", alternativeroute);
        Colors.put("footway", footway);
        Colors.put("cycleway", cycleway);
        Colors.put("motorway", motorway);
        Colors.put("trunk", trunk);
        Colors.put("primary", primary);
        Colors.put("secondary", secondary);
        Colors.put("tertiary", tertiary);
        Colors.put("railwaytunnel", railwaytunnel);
        Colors.put("railway", railway);
        Colors.put("pointofinterest", pointofinterest);
        Colors.put("bridleway", bridleway);
        Colors.put("airport", airport);
        Colors.put("moterwaytunnel", moterwaytunnel);
        Colors.put("green", green);
        Colors.put("red", Color.red);
        repaint();
    }

    public void setColorsToNegative() {
        for (Map.Entry<String, Color> entry : Colors.entrySet()) {
            if (!entry.getKey().equals("water")) {
                Colors.put(entry.getKey(),
                        new Color(255 - entry.getValue().getRed(),
                                255 - entry.getValue().getGreen(),
                                255 - entry.getValue().getBlue()));
            }
        }
        repaint();
    }

    public void setToGreyScale() {
        for (Map.Entry<String, Color> entry : Colors.entrySet()) {
            int greyScaleColor = (
                    entry.getValue().getRed() + entry.getValue().getGreen() + entry.getValue().getBlue() ) / 3;
                Colors.put(entry.getKey(),
                        new Color(greyScaleColor, greyScaleColor, greyScaleColor));
        }
        repaint();
    }

    public void darkerColors() {
        for (Map.Entry<String, Color> entry : Colors.entrySet()) {
            if (!entry.getKey().equals("water")) {
                Colors.put(entry.getKey(), entry.getValue().darker());
            }
        }
        repaint();
    }

    public void brighterColors() {
        for (Map.Entry<String, Color> entry : Colors.entrySet()) {
            if (!entry.getKey().equals("water")) {
                Colors.put(entry.getKey(), entry.getValue().brighter());
            }
        }
        repaint();
    }

    public double getCurrentZoomLVL() { return currentZoomLVL; }
}
