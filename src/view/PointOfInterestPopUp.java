package view;

import model.Model;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
/**
 * A window to make a new point of interest.
 */
public class PointOfInterestPopUp extends JFrame implements ActionListener{
    Model model;
    CanvasView canvas;
    Point2D point;
    final int maxLength = 40;
    /**
     * Constructs a PointOfInterestPopUp.
     * @param p is the new point of interests model coordinates.
     */
    public PointOfInterestPopUp(Model m, CanvasView cv, Point2D p) {
        super("Set Point Of Interest");
        this.model = m;
        this.canvas = cv;
        this.point = p;

        JTextField poi = new JTextField("Point of Interest");
        JTextField description = new JTextField();
        JButton button = new JButton("Add");

        description.setSize(200, 40);
        description.setEditable(true);
        poi.setEditable(false);

        this.setLayout(new BorderLayout());
        this.add(poi, BorderLayout.NORTH);
        this.add(description, BorderLayout.CENTER);
        this.add(button, BorderLayout.SOUTH);
        this.setSize(600, 400);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setLocation(MouseInfo.getPointerInfo().getLocation());

        /*A listener that checks if the description / name, is empty or too long, if it is the user
            will get a notification. Otherwise the point of interest will be made.*/
        button.addActionListener(e -> {
            if (!description.getText().equals("")) {
                if (description.getText().length() <= maxLength) {
                    model.addPointOfInterest(point, description.getText());
                    canvas.togglePointsOfInterestOn();
                    this.dispose();

                } else {
                    JOptionPane.showMessageDialog(null,
                            "Your point of interest's name, has to be less than " +  maxLength + " characters.");
                }

            } else {
                JOptionPane.showMessageDialog(null, "You have to give your point a name.");
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}

