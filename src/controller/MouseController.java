package controller;

import helpers.PointOfInterestShape;
import model.Model;
import view.CanvasView;
import view.EditPointOfInterestPopUp;
import view.PointOfInterestPopUp;
import view.Toolbar;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;
import static java.lang.Math.pow;

/**
 * Controls the actions taken, when the mouse is interacted with.
 */
public class MouseController extends MouseAdapter {
    private Model model;
    private CanvasView canvas;
    private Point2D lastMousePosition;
    private Toolbar toolbar;

    public MouseController(CanvasView cv, Model m, Toolbar t) {
        canvas = cv;
        model = m;
        toolbar = t;
        canvas.addMouseListener(this);
        canvas.addMouseWheelListener(this);
        canvas.addMouseMotionListener(this);
    }

    /**
     * Takes specific actions, when a specific key is released.
     * This is used, when panning by pressing and dragging the mouse.
     * @param e the mouse event
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        Point2D currentMousePosition = e.getPoint();
        double dx = currentMousePosition.getX() - lastMousePosition.getX();
        double dy = currentMousePosition.getY() - lastMousePosition.getY();
        canvas.pan(dx, dy);
        lastMousePosition = currentMousePosition;
    }

    /**
     * Takes specific actions, when the mouse is clicked.
     * This is used to add points of interest to the map or edit these (if the canvas already has focus).
     * @param e the mouse event
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (!canvas.hasFocus()) {
            canvas.grabFocus();
            return;
        }
        if (SwingUtilities.isLeftMouseButton(e)) {
            Point2D mousePoint = new Point2D.Double(e.getX(), e.getY());
            mousePoint = canvas.toModelCoords(mousePoint);
            /* If the mouse was over a point of interest, it makes an EditPointOfInterestPopUp
               otherwise it will make a PointOfInterestPopUp.
             */
            for (PointOfInterestShape p : canvas.getDrawnPointsOfInterest()) {
                if (p.getShape().contains(mousePoint)) {
                    new EditPointOfInterestPopUp(model, canvas, p.getIndexInPointsOfInterest());
                    return;
                }
            }
            new PointOfInterestPopUp(model, canvas, mousePoint);
        }
    }

    /**
     * Takes specific actions, when a the mouse is pressed.
     * This is used in conjuction with mouseDragged to do press and drag panning of the map.
     * @param e the mouse event
     */
    @Override
    public void mousePressed(MouseEvent e) {
        lastMousePosition = e.getPoint();
    }

    /**
     * Takes specific actions, when the mouse is moved.
     * This is used in order to show the nearest road in the toolbar.
     * @param e the mouse event
     */
    @Override
    public void mouseMoved (MouseEvent e){
        Point2D modelCoords = canvas.toModelCoords(e.getPoint());
        String output = model.getGraph().getNameOfClosestEdge(modelCoords.getX(), modelCoords.getY());
        toolbar.setNearestRoad(output);
    }

    /**
     * Takes specific actions, when the mouse wheel is moved.
     * This is used to zoom in and out of the map.
     * @param e
     */
    @Override
    public void mouseWheelMoved (MouseWheelEvent e){
        double factor = pow(1.1, e.getWheelRotation());
        canvas.zoom(factor, -e.getX(), -e.getY());
    }
}
