package controller.listeners;

import helpers.BearingCalculator;
import model.AddressFinder;
import model.Model;
import model.ShortestPath;
import model.WeightedGraph;
import view.CanvasView;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * Makes sure that the route is found and drawn on the canvas and that the directions is shown in the side menu, the
 * user presses.
 */
public class FindRouteListener implements ActionListener {
    private ArrayList<JToggleButton> buttonList;
    private ArrayList<JToggleButton> routeButtonList;
    private JTextArea directionsList;
    private JComboBox startComboBox;
    private JComboBox endComboBox;
    private AddressFinder addressFinder;
    private CanvasView canvasView;
    private Model model;
    private BearingCalculator bearingCalculator = new BearingCalculator();

    /**
     * Constructs a new FindRouteListener.
     * @param buttonList the list of vehicle buttons
     * @param routeButtonList the list of route buttons (fastest/shortest)
     * @param startComboBox the combobox containing the start address
     * @param endComboBox the combobox containing the destination address
     * @param m the model
     * @param cv the canvas
     * @param directionsList the textarea that is to show the route directions
     */
    public FindRouteListener(ArrayList<JToggleButton> buttonList, ArrayList<JToggleButton> routeButtonList,
                             JComboBox startComboBox, JComboBox endComboBox, Model m, CanvasView cv,
                             JTextArea directionsList) {
        this.buttonList = buttonList;
        this.routeButtonList = routeButtonList;
        this.startComboBox = startComboBox;
        this.endComboBox = endComboBox;
        this.model = m;
        this.directionsList = directionsList;
        addressFinder = new AddressFinder(m, cv);
        canvasView = cv;
    }

    /**
     *  Results in the route being drawn on the map and the directions being shown in the side menu, when the user presses
     *  find route.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // First the text of the two relevant toggle buttons is found (fastest/shortest & car, bike, walk)
        String buttonText = null;
        for(JToggleButton button : buttonList) {
            if(button.isSelected()) {
                buttonText = button.getText();
            }
        }
        String routeText = null;
        for(JToggleButton button : routeButtonList) {
            if(button.isSelected()) {
                routeText = button.getText();
            }
        }

        directionsList.setText("");

        // Finds the source and destination vertices by getting the closest vertex to the start- and destination coordinates,
        // where the chosen vehicle is allowed
        String startAddress = startComboBox.getEditor().getItem().toString();
        String destinationAddress = endComboBox.getEditor().getItem().toString();
        Point2D startCoordinates = addressFinder.findAddressCoordinates(startAddress);
        Point2D destinationCoordinates = addressFinder.findAddressCoordinates(destinationAddress);
        if (startCoordinates == null) {
            JOptionPane.showMessageDialog(null, "The start address, with the name " + startAddress + ", was not found! :-(");
            return;
        } else if (destinationCoordinates == null) {
            JOptionPane.showMessageDialog(null, "The end address, with the name " + destinationAddress + ", was not found! :-(");
            return;
        }

        addressFinder.panToRoute(startAddress);

        WeightedGraph.Vertex source = model.getGraph().getClosestVertex(startCoordinates, buttonText);
        WeightedGraph.Vertex destination = model.getGraph().getClosestVertex(destinationCoordinates, buttonText);

        // Finds the shortest/fastest route
        ShortestPath shortestPath = new ShortestPath(model);
        ArrayList<WeightedGraph.Edge> route = shortestPath.findPath(source, destination, buttonText, routeText);
        Path2D path = new Path2D.Double();
        path.moveTo(destination.getCoordinate().getX(), destination.getCoordinate().getY());
        WeightedGraph.Vertex currentVertex = destination;
        for(WeightedGraph.Edge edge : route) {
            if(currentVertex == edge.getStartVertex()) {
                currentVertex = edge.getEndVertex();
            }
            else {
                currentVertex = edge.getStartVertex();
            }
            path.lineTo(currentVertex.getCoordinate().getX(), currentVertex.getCoordinate().getY());
        }

        if(route.size() != 0){
            canvasView.drawRoute(true, path,
                    destination.getCoordinate());
            // Weight variables for the loop
            double weight = 0.0;
            double totalweight = 0.0;
            // Running through all the edges on the route from source to destination
            for(int i = route.size() - 1; i > 0; i--) {
                // Getting two edges at a time from the list to be able to figure out turns
                WeightedGraph.Edge edge = route.get(i);
                WeightedGraph.Edge secondEdge = route.get(i - 1);

                totalweight += secondEdge.getWeight();

                // Sets the direction or bearing of the second edge, the value is a double from [0-365[
                secondEdge.setDirection(bearingCalculator.calculateBearing(canvasView.toRealCoords(secondEdge.getFrom()),
                        canvasView.toRealCoords(secondEdge.getTo())));

                // Sets the direction or bearing of the very first edge on the route as seen from the source
                if (i == route.size() - 1) {
                    edge.setDirection(bearingCalculator.calculateBearing(canvasView.toRealCoords(edge.getFrom()),
                            canvasView.toRealCoords(edge.getTo())));
                    totalweight += edge.getWeight();
                    weight += edge.getWeight();
                    directionsList.append("Start on: " + edge.getName() + "." + "\n\n");
                }

                // If the second edge has the same name as the first edge, the weight of the second edge is added to the weight
                // variable, which is used later for length of a given road
                if (secondEdge.getName().length() > 0 && secondEdge.getName().equals(edge.getName())) {
                    weight += secondEdge.getWeight();
                    if(i != 1) {
                        continue;
                    }
                }

                // If the second edge does not have the same name as the first edge, the bearing of the edges is used to figure out,
                // which direction the vehicle is turning (left, right, forward)
                if (secondEdge.getName().length() > 0 && !secondEdge.getName().equals(edge.getName())) {
                    double bearingForEdge = edge.getDirection();
                    double bearingForSecondEdge = secondEdge.getDirection();
                    double oppositeBearingForEdge = bearingCalculator.calculateBearing(canvasView.toRealCoords(edge.getTo()),
                            canvasView.toRealCoords(edge.getFrom()));
                    double oppositeBearingForSecondEdge = bearingCalculator.calculateBearing(canvasView.toRealCoords(secondEdge.getTo()),
                            canvasView.toRealCoords(secondEdge.getFrom()));

                    // Switches the bearing with the opposite bearing, if the bearing is opposite the heading of the vehicle
                    if (edge.getStartVertex().getEdgeTo().equals(edge)) {
                        double temporary = oppositeBearingForEdge;
                        oppositeBearingForEdge = bearingForEdge;
                        bearingForEdge = temporary;
                    }
                    if (secondEdge.getStartVertex().getEdgeTo().equals(secondEdge)) {
                        bearingForSecondEdge = oppositeBearingForSecondEdge;
                    }

                    // Figure out the turn using the bearing of the edges
                    String turn;
                    if ((bearingForSecondEdge < oppositeBearingForEdge || (bearingForSecondEdge > bearingForEdge &&
                            bearingForSecondEdge > oppositeBearingForEdge && bearingForSecondEdge < 360 &&
                            bearingForSecondEdge - bearingForEdge < bearingForSecondEdge - oppositeBearingForEdge)) &&
                            (bearingForSecondEdge > bearingForEdge || bearingForSecondEdge < bearingForEdge &&
                                    bearingForSecondEdge < oppositeBearingForEdge && bearingForSecondEdge >= 0 &&
                                    oppositeBearingForEdge - bearingForSecondEdge < bearingForEdge -
                                            bearingForSecondEdge)) {
                        turn = "right";
                    }
                    else {
                        turn = "left";
                    }
                    if (bearingForEdge < 10 && bearingForSecondEdge > 348) {
                        bearingForEdge += 360;
                    }
                    else if(bearingForEdge > 348 && bearingForSecondEdge < 10) {
                        bearingForSecondEdge += 360;
                    }
                    if (bearingForEdge + 10 >= bearingForSecondEdge && bearingForEdge - 10 <= bearingForSecondEdge) {
                        turn = "forward";
                    }

                    // Checks if the distance is less than 1 km. Adds information about road to follow to the textarea in the side menu
                    if(weight < 1) {
                        weight = weight * 1000;
                        directionsList.append("Follow " + edge.getName() + " for " + String.format("%.0f", weight) +
                                "m." + "\n");
                    }
                    else {
                        directionsList.append("Follow " + edge.getName() + " for " + String.format("%.1f", weight) +
                                "km." + "\n");
                    }
                    directionsList.append("\n");

                    // Adds information about next turn to the textarea in the side menu
                    if(turn.equals("forward")) {
                        directionsList.append("Continue forward on " + secondEdge.getName() + "." + "\n");
                    }
                    else {
                        directionsList.append("Turn " + turn + " on " + secondEdge.getName() + "." + "\n");
                    }

                    directionsList.append("\n");
                    weight = secondEdge.getWeight();
                }

                // This is the end of the route, so information about arrival is added to the textarea in the side menu
                if(i == 1) {
                    if(weight < 1) {
                        weight = weight * 1000;
                        directionsList.append("Continue on " + secondEdge.getName() + " for " +
                                String.format("%.0f", weight) + "m till arrival at destination." + "\n");
                    }
                    else {
                        directionsList.append("Continue on " + secondEdge.getName() + " for " +
                                String.format("%.1f", weight) + "km till arrival at destination." + "\n");
                    }
                }
            }
            directionsList.append("\n");

            // Adds information about the length of the route to the textarea in the side menu
            if(totalweight < 1) {
                totalweight = totalweight * 1000;
                directionsList.append("Total length of route " + String.format("%.0f", totalweight) + "m." + "\n");
            }
            else {
                directionsList.append("Total length of route " + String.format("%.1f", totalweight) + "km." + "\n");
            }
        }
        else {
            // Repaints the canvas, if the list route is empty, so no route is showed on the canvas
            canvasView.repaint();
        }
    }
}
