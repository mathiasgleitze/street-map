package controller.listeners;

import helpers.AutoSuggestor;
import model.AddressFinder;
import model.Model;
import osm.OSMAddress;
import view.CanvasView;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Handles auto-suggestions when typing adresses into the comboboxes of the program.
 */
public class SearchTextFieldListener implements KeyListener {
    private JComboBox searchComboBox;
    private AddressFinder af;
    private String currentText;
    private String previousText;
    private ArrayList<OSMAddress> listOfAddresses = new ArrayList<>();

    /**
     * Constructs a new SearchTextFieldListener and a new Address within.
     * @param m the model
     * @param searchComboBox the combobox
     * @param c the canvas
     */
    public SearchTextFieldListener(Model m, JComboBox searchComboBox, CanvasView c) {
        this.searchComboBox = searchComboBox;
        this.af = new AddressFinder(m, c);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Takes specific action, when the specific key is released.
     * This is used, when typing in the search combo-boxes and returns 4 addresses that best matches the string in
     * the combobox.
     * @param e the key event
     */
    @Override
    public void keyReleased(KeyEvent e) {
        previousText = currentText;
        currentText = getInput();
        if (currentText.isEmpty()) {
            searchComboBox.removeAllItems();
            searchComboBox.hidePopup();
            return;
        }
        if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
            return;
        }
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            af.panToAddress(currentText);
            return;
        }
        if (!currentText.equals(previousText)){
            if (currentText.length() == 1)
                listOfAddresses = af.findListOfAddresses(currentText);

            if (listOfAddresses == null || listOfAddresses.isEmpty())
                return;

            ArrayList<String> foundStreets = af.findFourAddressNames(currentText, listOfAddresses);

            AutoSuggestor autoSuggestor = new AutoSuggestor(searchComboBox, foundStreets);
            autoSuggestor.updateSuggestions(foundStreets);
            searchComboBox.showPopup();
        }
    }

    /**
     * Returns the string from the ComboBox.
     * @return the string written in the combobox
     */
    private String getInput() {
        return searchComboBox.getEditor().getItem().toString();
    }
}
