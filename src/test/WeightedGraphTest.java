package test;

import org.junit.Before;
import org.junit.Test;
import osm.OSMRoad;
import osm.OSMWayType;
import model.WeightedGraph;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import static org.junit.Assert.*;

public class WeightedGraphTest {
    private ArrayList<OSMRoad> roads;
    private WeightedGraph wg;

    Point2D.Double point = new Point2D.Double(200, 200);
    Point2D.Double point2 = new Point2D.Double(400, 300);
    Point2D.Double point3 = new Point2D.Double(700, 900);
    Point2D.Double point4 = new Point2D.Double(0, 700);
    Point2D.Double point5 = new Point2D.Double(100, 550);
    Point2D.Double point6 = new Point2D.Double(550, 450);
    Point2D.Double point7 = new Point2D.Double(850, 350);

    OSMRoad road = new OSMRoad(OSMWayType.ROAD, "Vej1", false);
    OSMRoad road2 = new OSMRoad(OSMWayType.ROAD, "Vej2", false);
    OSMRoad road3 = new OSMRoad(OSMWayType.ROAD, "Vej3", true);
    OSMRoad road4 = new OSMRoad(OSMWayType.ROAD, "Vej4", false);
    OSMRoad road5 = new OSMRoad(OSMWayType.ROAD, "Vej5", false);
    OSMRoad road6 = new OSMRoad(OSMWayType.ROAD, "Vej6", false);
    OSMRoad road7 = new OSMRoad(OSMWayType.ROAD, "Vej7", true);

    @Before
    /**
     * We give values to every field an OSMRoad can have, so that they can be used as edges by the WeightedGraph constructor.
     * This setup method isn't needed, since we only test one method, but it makes the testmethod look cleaner.
     */
    public void setUp() throws Exception {
        roads = new ArrayList<>();

        road.from = point;
        road.to = point2;
        road.weight = 5.0;
        road.startNodeId = 121;
        road.endNodeId = 122;
        road.startNodeTrafficSignal = false;
        road.endNodeTrafficSignal = false;
        road.isDrivingAllowed = true;
        road.isCyclingAllowed = true;
        road.isWalkingAllowed = true;
        road.maxSpeed = 60.0;

        road2.from = point5;
        road2.to = point6;
        road2.weight = 5.0;
        road2.startNodeId = 125;
        road2.endNodeId = 126;
        road.startNodeTrafficSignal = false;
        road.endNodeTrafficSignal = false;
        road.isDrivingAllowed = true;
        road.isCyclingAllowed = true;
        road.isWalkingAllowed = true;
        road.maxSpeed = 60.0;

        road3.from = point2;
        road3.to = point6;
        road3.weight = 5.0;
        road3.startNodeId = 122;
        road3.endNodeId = 126;
        road.startNodeTrafficSignal = false;
        road.endNodeTrafficSignal = false;
        road.isDrivingAllowed = true;
        road.isCyclingAllowed = true;
        road.isWalkingAllowed = true;
        road.maxSpeed = 60.0;

        road4.from = point3;
        road4.to = point7;
        road4.weight = 5.0;
        road4.startNodeId = 123;
        road4.endNodeId = 127;
        road.startNodeTrafficSignal = false;
        road.endNodeTrafficSignal = false;
        road.isDrivingAllowed = true;
        road.isCyclingAllowed = true;
        road.isWalkingAllowed = true;
        road.maxSpeed = 60.0;

        road5.from = point4;
        road5.to = point5;
        road5.weight = 5.0;
        road5.startNodeId = 124;
        road5.endNodeId = 125;
        road.startNodeTrafficSignal = false;
        road.endNodeTrafficSignal = false;
        road.isDrivingAllowed = true;
        road.isCyclingAllowed = true;
        road.isWalkingAllowed = true;
        road.maxSpeed = 60.0;

        road6.from = point6;
        road6.to = point7;
        road6.weight = 5.0;
        road6.startNodeId = 126;
        road6.endNodeId = 127;
        road.startNodeTrafficSignal = false;
        road.endNodeTrafficSignal = false;
        road.isDrivingAllowed = true;
        road.isCyclingAllowed = true;
        road.isWalkingAllowed = true;
        road.maxSpeed = 60.0;

        road7.from = point3;
        road7.to = point5;
        road7.weight = 5.0;
        road7.startNodeId = 123;
        road7.endNodeId = 125;
        road.startNodeTrafficSignal = false;
        road.endNodeTrafficSignal = false;
        road.isDrivingAllowed = true;
        road.isCyclingAllowed = true;
        road.isWalkingAllowed = true;
        road.maxSpeed = 60.0;

        roads.add(road);
        roads.add(road2);
        roads.add(road3);
        roads.add(road4);
        roads.add(road5);
        roads.add(road6);
        roads.add(road7);

        wg = new WeightedGraph(roads);
    }

    /**
     * We have chosen not to test the getClosestVertex() method, since it is designed for much larger datasets, than what we have created.
     * Instead we test the method getNameOfClosestEdge(), to see if it works the way it should.
     */

    @Test
    public void getNameOfClosestEdge() throws Exception {
        assertEquals("Vej4", wg.getNameOfClosestEdge(750, 650));
        assertEquals("Vej1", wg.getNameOfClosestEdge(300, 250));
        assertEquals("Vej2", wg.getNameOfClosestEdge(300, 600));
        assertEquals("Vej5", wg.getNameOfClosestEdge(50, 650));
    }
}