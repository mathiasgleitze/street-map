package osm;
/**
 * A class to hold information about a road.
 */

import java.awt.geom.Point2D;
import java.io.Serializable;

public class OSMRoad implements Serializable {
    public OSMWayType type;
    public String name;
    public boolean isOneWay;
    public Point2D from;
    public Point2D to;
    public double weight;
    public long startNodeId;
    public long endNodeId;
    public boolean startNodeTrafficSignal;
    public boolean endNodeTrafficSignal;
    public boolean isDrivingAllowed;
    public boolean isCyclingAllowed;
    public boolean isWalkingAllowed;
    public double maxSpeed;

    public OSMRoad(OSMWayType type, String name, boolean isOneWay) {
        this.type = type;
        this.name = name;
        this.isOneWay = isOneWay;
    }
}
